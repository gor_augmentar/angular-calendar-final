import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CalendarService } from "app/services/calendar.service";
import { EventService } from "app/services/event.service";
import { EventDirective } from './services/event.directive';
import { CalendarHeaderMonthComponent } from './calendar-header-month/calendar-header-month.component';
import { DaysWithEventsMergerService } from "app/services/days-with-events-merger.service";
import { CalendarMonthComponent } from './calendar-month/calendar-month.component';
import { CalendarWeekComponent } from './calendar-week/calendar-week.component';
import { CalendarHeaderWeekComponent } from './calendar-header-week/calendar-header-week.component';

@NgModule({
  declarations: [
    AppComponent,
    EventDirective,
    CalendarHeaderMonthComponent,
    CalendarMonthComponent,
    CalendarWeekComponent,
    CalendarHeaderWeekComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [CalendarService, EventService,DaysWithEventsMergerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
