import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { EventObject } from '../models/event.model.class';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EventService {
  private params: URLSearchParams = new URLSearchParams();
  private apiKey = 'AIzaSyAWCbbo7_Q_Brtvw9KNmCsyCGTmR_gHEz8';
  private apiUrl = 'https://www.googleapis.com/calendar/v3/calendars/gurgenmnxv@gmail.com/events';
  constructor(private http: Http) {
    this.params.set('key', this.apiKey);
   }
  public getEvents(year, month, day?): Observable<EventObject[]> {
     if (!day) {
        const date = new Date(year, month, 1);
        this.params.set('timeMin', date.toISOString());
        this.params.set('timeMax', new Date(date.setMonth(date.getMonth() + 1)).toISOString());
     } else {
      let date = new Date(year, month, day);
      date = new Date(date.setDate(date.getDate() - date.getDay()));
      this.params.set('timeMin', date.toISOString());
      this.params.set('timeMax', new Date(date.setDate(date.getDate() + 7)).toISOString());
     }
     return this.http.get(this.apiUrl, {search: this.params})
      .map(resp => resp.json().items)
      .map(items => items.map(this.toEventObject));
  }

  toEventObject(event): Object {
    const dayBinder = event.start.dateTime || event.start.date;
    return {
          title : event.summary,
          dayBinder: dayBinder.substr(0, 10),
          description : event.description,
          startDate: event.start.dateTime || event.start.date,
          location : event.location
    }
  }
}
