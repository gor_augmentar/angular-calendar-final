import { Injectable } from '@angular/core';
import { CalendarService } from 'app/services/calendar.service';
import { EventService } from 'app/services/event.service';
import { DayObject } from '../models/calendar.model.class';

@Injectable()
export class DaysWithEventsMergerService {
  public year: number;
  public month: number;
  public day: number;
  public searchText: string = '';
  constructor(private claendareService: CalendarService, private eventService: EventService) {
    const date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth();
    this.day = date.getDate();
  }
  public getMonthWithEvents(year, month): Promise<DayObject[]> {
    return new Promise((resolve, reject) => {
      this.claendareService.getMonth(year, month).then(days => {
        this.eventService.getEvents(year, month).subscribe(events => {
          for (let event of events){
            for (let day of days) {
              if (day.dayDate == event.dayBinder && event.title.toLowerCase().indexOf(this.searchText.toLowerCase()) >= 0){
                day.events.push(event);
              }
            }
          }
          days = this.getWeeksFromMonth(days);
          resolve(days);
        })
      })
    })
  }

  public getWeekWithEvents(year, month, day): Promise<DayObject[]> {
    return new Promise((resolve, reject) => {
      this.claendareService.getWeek(year, month, day)
          .then(days => {
              this.eventService.getEvents(year, month, day).subscribe(events => {
                for (let event of events){
                  for (let day of days) {
                    if (day.dayDate == event.dayBinder && event.title.toLowerCase().indexOf(this.searchText.toLowerCase()) >= 0){
                      day.events.push(event);
                    }
                  }
                }
                resolve(days);
            })
        })
    })
  }

  private getWeeksFromMonth(monthDays) {
    const array = [];
    const firstSliceIndex = 7 - monthDays[0].day;
    const firstWeekDays = monthDays.splice(0, firstSliceIndex);
    while (firstWeekDays.length < 7) {
      firstWeekDays.unshift(new DayObject());
    }
    array.push(firstWeekDays);
    while (monthDays.length > 0) {
      const weekDays = monthDays.splice(0, 7);
      array.push(weekDays);
    }
    while (array[array.length - 1].length < 7) {
      array[array.length - 1].push(new DayObject);
    }
    return array;
  }
}
