import { Directive, ElementRef, Renderer, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[dataBind]'
})
export class EventDirective implements OnInit {
  @Input('dataBind') dataBind;
  @Input('index') index: number;
  colors: Array<string> = ['rgb(147, 148, 197)', '#ffb878', '#7E8F7C'];
  constructor(private el: ElementRef, private renderer: Renderer) { }

  ngOnInit() {
    this.renderer.setElementAttribute(this.el.nativeElement, 'data-description', this.dataBind.description);
    this.renderer.setElementAttribute(this.el.nativeElement, 'data-location', this.dataBind.location);
    this.renderer.setElementAttribute(this.el.nativeElement, 'data-date', this.dataBind.dayBinder);
    this.renderer.setElementAttribute(this.el.nativeElement, 'data-title', this.dataBind.title);
    this.renderer.setElementStyle(this.el.nativeElement, 'background-color', this.colors[this.index]);
  }
}
