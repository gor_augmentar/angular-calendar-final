import { Injectable } from '@angular/core';
import { DayObject } from '../models/calendar.model.class';
import { EventService } from 'app/services/event.service';

@Injectable()
export class CalendarService {
  public months_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                        'August', 'September', 'October', 'November', 'December'];
  public weekDays: Array<DayObject> = [];
  public monthDays:  Array<DayObject> = [];
  constructor() {}

  public getWeek(year, month, day): Promise<DayObject[]> {
    return new Promise((resolve, reject) => {
      const date = new Date(year, month, day);
      const lastSunday = new Date(date.setDate(date.getDate() - date.getDay()));
      const weekDays = this.DayObjectCreator(lastSunday, 7);
      resolve(weekDays);
    })
  }

  public getMonth(year, month): Promise<DayObject[]> {
    return new Promise((resolve, reject) => {
      const monthDaysCount = new Date(year, month + 1, 0).getDate();
      const monthFirstDay = new Date(year, month, 1);
      const monthDays = this.DayObjectCreator(monthFirstDay, monthDaysCount);
      resolve(monthDays);
    })
  }

  private DayObjectCreator(firstDay: Date, forCount: number): Array<DayObject> {
    const days = [];
    let day;
    for (let i = 0; i < forCount; i++) {
      day = new DayObject();
      day.dayNumber = firstDay.getDate();
      day.dayDate = this.stringFromDate(firstDay);
      day.year = firstDay.getFullYear();
      day.month = firstDay.getMonth();
      day.day = firstDay.getDay();
      day.events = [];
      days.push(day);
      firstDay.setDate(day.dayNumber + 1);
    }
    return days;
  }

  public getMonthsName(month, year): Object {
    const currentMonthName = this.months_name[month];
    let prevMonthName, nextMonthName;
    if (month === 0) {
      prevMonthName = this.months_name[11];
    } else {
      prevMonthName = this.months_name[month - 1];
    }

    if (month === 11) {
      nextMonthName = this.months_name[0];
    } else {
      nextMonthName = this.months_name[month + 1];
    }
    return {
      'currentMonthName': currentMonthName,
      'nextMonthName': nextMonthName,
      'prevMonthName': prevMonthName,
      'month': month,
      'year': year
    }
  }

  public getWeekInfo(year, month, day): Object {
    const date = new Date(year, month, day);
    const lastSunday = new Date(date.setDate(date.getDate() - date.getDay()));
    const lastSundayName = this.getMonthAndDay(lastSunday);
    let saturday = new Date(lastSunday);
    saturday = new Date(saturday.setDate(saturday.getDate() + 6));
    const saturdayName = this.getMonthAndDay(saturday);
    const leftWeekName = this.getMonthAndDay(new Date(lastSunday.setDate(lastSunday.getDate() - 7)));
    const rightWeekName = this.getMonthAndDay(new Date(saturday.setDate(saturday.getDate() + 1)));
    return {
      'lastSundayName': lastSundayName,
      'saturdayName': saturdayName,
      'leftWeekName': leftWeekName,
      'rightWeekName': rightWeekName,
      'year': year
    }
  }

  private getMonthAndDay(date: Date) {
    return this.months_name[date.getMonth()] + ' ' + date.getDate();
  }

  private stringFromDate(date: Date): string {
    return date.getFullYear() + '-' + this.appendZero(date.getMonth() + 1) + '-' + this.appendZero(date.getDate());
  }

  private appendZero(number: number): string {
      return number < 10 ? '0' + String(number) : String(number);
  }
}
