import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarHeaderWeekComponent } from './calendar-header-week.component';

describe('CalendarHeaderWeekComponent', () => {
  let component: CalendarHeaderWeekComponent;
  let fixture: ComponentFixture<CalendarHeaderWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarHeaderWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarHeaderWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
