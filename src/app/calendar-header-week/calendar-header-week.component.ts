import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DaysWithEventsMergerService } from 'app/services/days-with-events-merger.service';

@Component({
  selector: 'app-calendar-header-week',
  templateUrl: './calendar-header-week.component.html',
  styleUrls: ['./calendar-header-week.component.css']
})
export class CalendarHeaderWeekComponent implements OnInit {
  @Input() weekInfo: Object;
  @Output() weekChanged = new EventEmitter();
  @Output() yearChanged = new EventEmitter();
  @Output() searchInputed = new EventEmitter();
  constructor(private calendarService: DaysWithEventsMergerService) { }

  ngOnInit() {
  }

  changeWeek(step): void {
    this.weekChanged.emit({'step' : step});
  }

  changeYear(step): void {
    this.yearChanged.emit({'step': step});
  }

  search(text): void {
    this.searchInputed.emit();
  }
}
