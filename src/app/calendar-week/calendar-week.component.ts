import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DaysWithEventsMergerService } from 'app/services/days-with-events-merger.service';
import { DayObject } from '../models/calendar.model.class';
import { CalendarService } from 'app/services/calendar.service';

@Component({
  selector: 'app-calendar-week',
  templateUrl: './calendar-week.component.html',
  styleUrls: ['./calendar-week.component.css']
})
export class CalendarWeekComponent implements OnInit {
  @Output() changeView = new EventEmitter;
  public year: number;
  public month: number;
  public dayNumber: number;
  public dataReady: boolean = false;
  public childData: Object;
  public daysObject: Array<DayObject>;
  constructor(private calendarService: DaysWithEventsMergerService, private calendar: CalendarService) {
    this.initDate(new Date);
  }

  ngOnInit() {
    this.init();
  }

  init(): void {
    this.getChildData();
    this.initData();
  }

  initData(): void {
    this.calendarService.getWeekWithEvents(this.year, this.month, this.dayNumber).then(items => {
      this.daysObject = items;
      this.dataReady = true;
    })
  }
  getChildData(): void {
    this.childData = this.calendar.getWeekInfo(this.year, this.month, this.dayNumber);
  }

  weekChanged(step) {
    step.step === 'next' ? this.dayNumber += 7 : this.dayNumber -= 7;
    this.initDate(new Date(this.year, this.month, this.dayNumber));
    this.init();
  }
  yearChanged(step): void {
    step.step === 'next' ? this.year += 1 : this.year -= 1;
    this.init();
  }

  initDate(date): void {
    this.year = date.getFullYear();
    this.month = date.getMonth();
    this.dayNumber = date.getDate();
  }

  searchInputed() {
    this.init();
  }

  viewChanged(viewCase: boolean) {
    this.changeView.emit({'case': viewCase});
  }
}
