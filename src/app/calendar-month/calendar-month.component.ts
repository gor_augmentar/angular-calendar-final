import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DaysWithEventsMergerService } from "app/services/days-with-events-merger.service";
import { DayObject } from '../models/calendar.model.class';
import { CalendarService } from "app/services/calendar.service";

@Component({
  selector: 'app-calendar-month',
  templateUrl: './calendar-month.component.html',
  styleUrls: ['./calendar-month.component.css']
})
export class CalendarMonthComponent implements OnInit {
  @Output() changeView = new EventEmitter;
  public year: number;
  public month: number;
  public daysObject: Array<DayObject>;
  public dataReady: boolean = false;
  public childData: Object;
  public popupData: Object;
  public popupDisplay: string = 'none';
  public weekNames : Array<string> = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  constructor(private calendarService: DaysWithEventsMergerService, private calendar: CalendarService ) {
    const date = new Date();
    this.year = date.getFullYear();
    this.month = date.getMonth();
   }

  ngOnInit() {
    this.init();
  }

  init(): void {
    this.getChildData();
    this.initData();
  }

  initData(): void {
    this.calendarService.getMonthWithEvents(this.year, this.month).then(items => {
      this.daysObject = items;
      this.dataReady = true;
    })
  }

  getChildData(): void {
    this.childData = this.calendar.getMonthsName(this.month, this.year);
  }

  searchInputed() {
    this.init();
  }

  monthChanged(step) {
    step.step === 'next' ? this.nextMonth() : this.prevMonth();
  }

  yearChanged(step): void {
    step.step === 'next' ? this.year += 1 : this.year -= 1;
    this.init();
  }

  nextMonth() {
    if (this.month === 11) {
      this.year += 1;
      this.month = 0;
    } else {
       this.month += 1;
    }
    this.init();
  }

  prevMonth() {
    if (this.month === 0) {
      this.year -= 1;
      this.month = 11;
  } else {
      this.month -= 1;
    }
    this.init();
  }

  eventHovered(event): void {
    this.popupDisplay = 'flex';
    this.popupData = {
      'top': event.clientY + 'px',
      'left': event.clientX + 'px',
      'title': event.target.dataset.title,
      'description': event.target.dataset.description,
      'location': event.target.dataset.location,
      'date': event.target.dataset.date
    }
  }

  eventUnHovered(): void {
    this.popupDisplay = 'none';
  }

  viewChanged(viewCase: boolean) {
    this.changeView.emit({'case': viewCase});
  }
}
