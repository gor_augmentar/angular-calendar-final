import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DaysWithEventsMergerService } from 'app/services/days-with-events-merger.service';

@Component({
  selector: 'app-calendar-header-month',
  templateUrl: './calendar-header-month.component.html',
  styleUrls: ['./calendar-header-month.component.css']
})
export class CalendarHeaderMonthComponent implements OnInit {
  @Input() dateInfo: Object;
  @Output() monthChanged = new EventEmitter();
  @Output() yearChanged = new EventEmitter();
  @Output() searchInputed = new EventEmitter();
  constructor(private calendarService: DaysWithEventsMergerService) { }

  ngOnInit() {
  }

  changeMonth(step: string): void {
    this.monthChanged.emit({'step': step});
  }
  changeYear(step): void {
    this.yearChanged.emit({'step': step});
  }
  search(text): void {
    this.searchInputed.emit();
  }
}
