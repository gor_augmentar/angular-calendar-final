import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarHeaderMonthComponent } from './calendar-header-month.component';

describe('CalendarHeaderMonthComponent', () => {
  let component: CalendarHeaderMonthComponent;
  let fixture: ComponentFixture<CalendarHeaderMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarHeaderMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarHeaderMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
