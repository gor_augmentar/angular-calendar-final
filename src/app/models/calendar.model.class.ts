import { EventObject } from './event.model.class';
export class DayObject {
    public dayNumber: number;
    public dayDate: string;
    public year: number;
    public month: number;
    public day: number;
    public events: Array<EventObject>;
}
