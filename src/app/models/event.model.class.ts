export class EventObject {
    public title: string;
    public description: string;
    public startDate: string;
    public location: string;
    public dayBinder: string;
}
